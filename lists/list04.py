#!/usr/bin/env python3

'''Alta3 Research | TPatrick
   Reading data out of a file and into a list format.'''

def main():
    '''reading data out of a file'''

    iplist = []

    with open('ipdata.txt', 'r') as ipdata:
        for line in ipdata:
            iplist.append(line.rstrip("\n"))

    print(iplist)

    for ip in iplist:
        print(ip)

if __name__ == "__main__":
    main()

